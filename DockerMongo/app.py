"""
Author: Shikun Lin
UO CIS322 18F

This is a flask server which can calculate the control times.
And save the data to database. Then, send data from database to
front end and print on web page

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging



###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

#Holder for the data whcih need to be saved
data_saved = ""

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    #Get the data from front end
    km = request.args.get('km', 999, type=float)
    beg_date = request.args.get("beg_date",type = str)
    beg_time = request.args.get("beg_time",type = str)
    distance = request.args.get("distance",type = int)
    location = request.args.get("location",type=str)
    # Create a arrow object
    date_time = arrow.utcnow()
    date = arrow.get(beg_date+" "+beg_time,'YYYY-MM-DD HH:mm').isoformat()
    #Set the value based on front end data
    date_time = arrow.get(date)
    #Set the time zone
    date_time=date_time.replace(tzinfo='US/Pacific')
    date_time = date_time.isoformat()


    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, date_time)
    close_time = acp_times.close_time(km, distance, date_time)
    result = {"open": open_time, "close": close_time}


    #Add the new data to the holder in correct in html syntax
    global data_saved

    data_saved = data_saved + 'Location: '+ location + '<br/>'\
        +'     Brevet distance: ' + str(distance) + '<br/>'\
        + '     KM: ' + str(km) + '<br/>'\
        + '     Open: ' + open_time + '<br/>'\
        + '     Close: '  + close_time + '<br/>' + '<br/>'
    return flask.jsonify(result=result)

#Send the json with data into database
@app.route('/_submit', methods=['POST'])
def new():
    global data_saved

    #make an Json for data
    data = {'data':data_saved}
    #insert to database
    if(len(data["data"])>0):
        db.tododb.insert_one(data)
    #reset the holder to empyty string
    data_saved = ""
    return flask.render_template('calc.html')

#Display the data which saved in database on webpage
@app.route('/_display',methods=['POST'])
def display():
    #Holder for the data need to transfer to fornt end
    items = [{"data":""}]

    #If the database is not empyty, then get the data from database
    if db.tododb.count() == 0:
        items = [{"data":""}]
    else:
        _items = db.tododb.find()
        items = [item for item in _items]
        #delete the Mongodb id from the data which send to front end
        for item in items:
            del item['_id']
    #rendering todo.html with data
    return render_template('todo.html', items=items)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
