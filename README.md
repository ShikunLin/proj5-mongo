# Project 5: Brevet time calculator with Ajax and MongoDB

Author:Shikun Lin  
Contact address: shikunl@uoregon.edu  

## Description  
This peogram can create a flask project. This project can caculate the the ACP times for control points. The  project allows users save the data (open time, close time, location name, brevet distance, and control point distace) into Mongo database with "Submit" button on homepage. Also, it allows users to display the data which saved in database with "Display" button on homepage.  

#How to Run
1. cd to the "DockerMongo" directory.  
2. Copy the credentials file into "DockerMongo".  
3. Using "docker-compose up" to build the project.  
4. Launch http://127.0.0.1:5000 using web browser